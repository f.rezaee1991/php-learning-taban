<?php

// ====================================================================================================
//                                        ERROR
// ====================================================================================================

//-------------------
/*
میخواهیم یک فایل را باز کنیم :

$file=fopen("welcome.txt","r");
 ==> فایل وجود نداره پس خطا میده
 //Warning: fopen(welcome.txt) [function.fopen]: failed to open stream:No such file or directory in C:\webfolder\test.php on line 2 
 اول بیایم بررسی کنیم که فایل وجود داره یا نه تا کاربر خطا رو نبینه
if(!file_exists("welcome.txt")) {
    die("فایل پیدا نشد");
  } else {
    $file=fopen("welcome.txt","r");
  }
  */
//-------------------
//-------------------

/*
ثابت های از پیش تعریف شده
در هسته پی اپچ پی تعریف شده اند
*/
//-------------------
// خطا های احتمالی در برنامه نویسی php : 

// 1- fetal error خطاهای بحرانی
// Method();
// echo "ok"; // اجرای برنامه متوقف میشه

// 2-parse error 
// echo "this is parse err (synnax err)" // اجرای برنامه متوقف میشه

// 3- warning  فقط هشدار 
//echo 10 / 0;   //Warning: Division by zero in C:\Program Files\Ampps\www\tamrin\tamrin error php.php on line 93
//  اجرای برنامه متوقف نمیشه

//4- Notices  == >  (جلب توجه)خودکار توسط خود پی اچ پی یا خود برنامه نویس که از تریگر ارور استفاده می کند ساخته میشه
//     $x += 1;
//     echo "RESULT: ". $x;  // مانع از اجرای دستور نمیشه

// 5- trigger_error 
// $gholam = -90;
// if ($gholam < 0) {
//     trigger_error('عدد منفی است');  //  Notice: عدد منفی است in C:\Program Files\Ampps\www\tamrin\tamrin error php.php on line 103
// }

//6 - Deprecated error ==> منسوخ شدن دستور مورد نظر  یعنی در نسخه های بالاتر از 5 دیگه این دستور استفاده نمیشه
// میشه معادل سازی کرد

//-------------------
// The error_reporting() function specifies which errors are reported.   
// اینکه چه خطایی گزارش بشه یا نادیده گرفته بشه با ارور ریپورتینگ کنترل میشه 

// Turn off error reporting
// error_reporting(0);

// Report runtime errors
// error_reporting(E_ERROR | E_WARNING | E_PARSE);
//The error_reporting() function sets the error_reporting directive at runtime.  زمان اجرا

// Report all errors
// error_reporting(E_ALL);

// تفاوت توابع ()error_reporting و ()ini_set
//  هر دو تابع کار یکسانی انجام می‌دهند:

// display_errors = on; خطاها نمایش داده میشه  در محیط تولید بهتره آف باشه در محیط توسعه بهتره آن باشه

// phpinfo();  ==> اطلاعات پی اچ پی نمایش داده میشه برای پیدا کردن آدرس فایل  پی اچ پی دات اینی  این دستور را اجرا می کنیم
// ini_set('display_errors',"on") // اینی ست تنظیمات را اور رایت می کند 

// 
//-------------------

// error_log file  : این فایل برای ذخیره خطاهای وب سایت شما می باشد.
// فایل error_log به‌وسیله هاستینگ شما ساخته می‌شود. error_log یک فایل متنی است که غیرقابل اجرا می‌باشد
// log_errors : لاگ زدن خطاها که میشه خاموش روشنش کرد یک متغیر است.
// error_log فعال کردن : log_errors = on;
//-------------------
/* set_error_handler() Function

توسعه دهنده با این تابع میتونه روش اختصاصی خودش برای مدیریت خطاها در زمان اجرا ی برنامه تعریف کند 
Set a user-defined error handler function with the set_error_handler() function, and trigger an error (with trigger_error()):

<?php
// A user-defined error handler function
function myErrorHandler($errno, $errstr, $errfile, $errline) {
    echo "<b>Custom error:</b> [$errno] $errstr<br>";
    echo " Error on line $errline in $errfile<br>";
}

// Set user-defined error handler function
set_error_handler("myErrorHandler");

$test=2;

// Trigger error
if ($test>1) {
    trigger_error("A custom error has been triggered");
}
?>
errno – اولین پارامتر، errno ، شامل سطح و شدت خطای تولید شده است که به صورت یک عدد صحیح یا integer به تابع فرستاده می شود.
errstr - دومین پارامتر، errstr، پیغام خطا را که اطلاعاتی راجع به آن دربردارد، به صورت یک رشته تعیین می کند.
errfile - پارامتر سوم اختیاری است و اسم فایلی که خطا در آن رخ داده را در قالب یک رشته شامل می شود.
errline - این پارامتر اختیاری شماره ی خطی که خطا در آن رخ داده را در قالب یک عدد صحیح مشخص می کند.
errcontext - این پارامتر اختیاری، آرایه ای است که به متغیرها و مقدار متناظر آن ها در اسکریپت برنامه که خطا در آن صادر گردیده، اشاره دارد.


  کلاس ارور : Error is the base class for all internal PHP errors.
تابع set_exception_handler() نامِ یک تابعِ تعریف شده توسط کاربر را می گیرد.و از آن برای همه استثناهایی که بلوک catch برایشان تعریف نشده است استفاده می کند.

*/

//-------------------
/* 
سلسله مراتب ارورها
Error hierarchy ¶
Throwable
    Error
        ArithmeticError
            DivisionByZeroError
        AssertionError
        CompileError
            ParseError
        TypeError
            ArgumentCountError
        ValueError
        UnhandledMatchError
    Exception
...

...
*/
//-------------------


// ====================================================================================================
//                                        Exceptions 
// ====================================================================================================
// مثال :
// دوچرخه خراب شده آیا دیگه نمیتونیم بریم مدرسه؟ خب با اتوبوس میریم
//-------------------
// $firstNumber = 10;
// $secondNumber = 0;
// $result = $firstNumber / $secondNumber;
// echo $firstNumber . "/" . $secondNumber . " = " . $result; //  
// Warning: Division by zero in C:\Program Files\Ampps\www\tamrin\tamrin error php.php on line 9
// 10/0 = INF
//-------------------

// $firstNumber = 10;
// $secondNumber = 0;
// if ($secondNumber != 0) {
//     $result = $firstNumber / $secondNumber;
// } else {
//     // echo (new Exception(' taghsim bar 0 anjam shode')); // 00Exception: taghsim bar 0 anjam shode in C:\Program Files\Ampps\www\tamrin\tamrin error php.php:18 Stack trace: #0 {main}
//     throw new Exception(' taghsim bar 0 anjam shode'); // ایجاد یک شی از کلاس اکسپشن و نمایش پیغام دستی
// } uncaught exception روی میده یک استثنایی مدیریت نشده اتفاق میفته
// As of PHP 8.0.0, the throw keyword is an expression and may be used in any expression context

//-------------------
/*$firstNumber = 10;
$secondNumber = 0;
try {
    if ($secondNumber != 0) {
        $result = $firstNumber / $secondNumber;
    } else {

        throw new Exception(' تقسیم بر صفر انجام شده است');
    }
} catch (Exception $a) { // نامگذاری آبجکت از کلاس اکسپشن
    echo $a->getMessage(); // تابع درون شی ساخته شده برای نمایش
} // ارور هندل شد با نمایش پیغام خواسته شده 
*/


//-------------------
// اعتبار سنجی شماره موبایل بصورت ساده
/*
// $mobile = "09123456789"; // تعداد ارقام 11 تاست و رقم اول موبایل صفر است
// $mobile = "00009123456789"; // تعداد ارقام موبایل 11 تا نیست
$mobile = "99123456789"; // تعداد ارقام 11 تاست اولین رقم از موبایل باید صفر باشد

try {
    if (strlen($mobile) == 11) {
        echo ' تعداد ارقام 11 تاست ';
    } else {
        throw new Exception(' تعداد ارقام موبایل 11 تا نیست');
    }

    if ($mobile['0'] == "0") {
        echo '  و رقم اول موبایل صفر است';
    } else {
        throw new Exception(' اولین رقم از موبایل باید صفر باشد');
    }
} catch (Exception $e) {
    echo $e->getMessage();

finally {
    echo "اینجا فاینالی است";  
}
*/
//-------------------
/*
// متدهای شی exception $e
$e->getMessage();
$e->getCode(); // get code of exception
$e->getFile(); // source file name
$e->getLine(); // source line 
$e->getTrace(); // an array of backtrace
$e->getTraceAsString(); // an array of backtrace as string
*/
//-------------------

// ساخت یک کلاس استثنا  : Creating a Custom Exception Class

//   به ارث می بردException از کلاس 
//   باید توابعی داشته باشه که هنگام رخ دادن فراخوانی بشن
/*
$mobile = '09999999121234567';
try {
    if (strlen($mobile) == 11) {
        echo 'تعداد ارقام 11 تا است ';
    } else {
        throw new MyException('تعداد ارقام صحیح نیست');
    }
} catch (MyException $e) {
    echo $e->showMsg();
}


class MyException extends Exception
{
    public function showMsg()
    {
        echo ' من یک پیام از کلاس مای اکسپشن هستم';
    }
}
*/
//-------------------
// استثنا های چندگانه :  Multiple Exceptions

/*

class customException extends Exception
{
    public function errorMessage()
    {
        //error message
        $errorMsg = 'Error on line ' . $this->getLine() . ' in ' . $this->getFile()
            . ': <b>' . $this->getMessage() . '</b> آدرس ایمیل معتبر نیست';
        return $errorMsg;
    }
}

$email = "someone@example.com";

try {
    //check if
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) { // چک میکنه که ایمیل معتبره یا نه 
        //throw exception if email is not valid
        throw new customException($email);
    }
    //check for "example" in mail address
    if (strpos($email, "example") !== FALSE) {
        throw new Exception("$email is an example e-mail");
    }
} catch (customException $e) {
    echo $e->errorMessage();
} catch (Exception $e) {
    echo $e->getMessage();
}
*/

/* 
کد بالا دو شرط را آزمایش می کند و اگر هیچ کدام از شرط ها برقرار نباشد یک استثنا پرتاب می کتد.

کلاس ()customException به عنوان یک استثنا کلاس exception قدیمی ساخته شده است. از این طریق از تمام متغیر ها و توابع کلاس قدیمی ارث بری می کند.
تابع ()errorMessage یک پیغام خطا بر می گرداند اگر آدرس ایمیل نامعتبر باشد
متغیر email$ برای یک رشته آدرس ایمیل معتبر تنظیم شده اما مقدار رشته “example” را دارد
بلوک try اجرا شده و در اولین شرط استثنا پرتاب نشده است
دومین شرط یک استثنا را پرتاب خواهد کرد زیرا آدرس ایمیل معتبر نیست
بلوک catch استثنا را گرفته و پیغام خطا را نمایش می دهد*/

//-------------------


// var_dump($_SERVER); 
// یک آرایه می دهد
//array(44) { ["TMP"]=> string(26) "C:/Program Files/Ampps/tmp" ["HTTP_HOST"]=> string(9) "localhost" ["HTTP_CONNECTION"]=> string(10) "keep-alive" ["HTTP_CACHE_CONTROL"]=> string(9) "max-age=0" ["HTTP_SEC_CH_UA"]=> string(64) ""Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"" ["HTTP_SEC_CH_UA_MOBILE"]=> string(2) "?0" ["HTTP_UPGRADE_INSECURE_REQUESTS"]=> string(1) "1" ["HTTP_USER_AGENT"]=> string(115) "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36" ["HTTP_ACCEPT"]=> string(135) "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" ["HTTP_SEC_FETCH_SITE"]=> string(11) "same-origin" ["HTTP_SEC_FETCH_MODE"]=> string(8) "navigate" ["HTTP_SEC_FETCH_USER"]=> string(2) "?1" ["HTTP_SEC_FETCH_DEST"]=> string(8) "document" ["HTTP_REFERER"]=> string(24) "http://localhost/tamrin/" ["HTTP_ACCEPT_ENCODING"]=> string(17) "gzip, deflate, br" ["HTTP_ACCEPT_LANGUAGE"]=> string(23) "en-US,en;q=0.9,fa;q=0.8" ["HTTP_COOKIE"]=> string(52) "SOFTCookies8042_sid=oeAhOsXZHMFMdmZpwD2Out2YOgWSrBTm" ["PATH"]=> string(455) "C:\Users\farzad\AppData\Local\Programs\Python\Python39\Scripts\;C:\Users\farzad\AppData\Local\Programs\Python\Python39\;C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem;C:\WINDOWS\System32\WindowsPowerShell\v1.0\;C:\Program Files\Microsoft VS Code\bin;C:\WINDOWS\System32\OpenSSH\;C:\Program Files\nodejs\;C:\ProgramData\chocolatey\bin;C:\Program Files\Git\cmd;C:\Users\farzad\AppData\Local\Microsoft\WindowsApps;C:\Users\farzad\AppData\Roaming\npm" ["SystemRoot"]=> string(10) "C:\WINDOWS" ["COMSPEC"]=> string(27) "C:\WINDOWS\system32\cmd.exe" ["PATHEXT"]=> string(62) ".COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH;.MSC;.PY;.PYW" ["WINDIR"]=> string(10) "C:\WINDOWS" ["SERVER_SIGNATURE"]=> string(0) "" ["SERVER_SOFTWARE"]=> string(47) "Apache/2.4.41 (Win64) OpenSSL/1.1.1c PHP/7.3.11" ["SERVER_NAME"]=> string(9) "localhost" ["SERVER_ADDR"]=> string(3) "::1" ["SERVER_PORT"]=> string(2) "80" ["REMOTE_ADDR"]=> string(3) "::1" ["DOCUMENT_ROOT"]=> string(26) "C:/Program Files/Ampps/www" ["REQUEST_SCHEME"]=> string(4) "http" ["CONTEXT_PREFIX"]=> string(0) "" ["CONTEXT_DOCUMENT_ROOT"]=> string(26) "C:/Program Files/Ampps/www" ["SERVER_ADMIN"]=> string(15) "admin@localhost" ["SCRIPT_FILENAME"]=> string(54) "C:/Program Files/Ampps/www/tamrin/tamrin error php.php" ["REMOTE_PORT"]=> string(5) "52047" ["GATEWAY_INTERFACE"]=> string(7) "CGI/1.1" ["SERVER_PROTOCOL"]=> string(8) "HTTP/1.1" ["REQUEST_METHOD"]=> string(3) "GET" ["QUERY_STRING"]=> string(0) "" ["REQUEST_URI"]=> string(32) "/tamrin/tamrin%20error%20php.php" ["SCRIPT_NAME"]=> string(28) "/tamrin/tamrin error php.php" ["PHP_SELF"]=> string(28) "/tamrin/tamrin error php.php" ["REQUEST_TIME_FLOAT"]=> float(1617875963.365) ["REQUEST_TIME"]=> int(1617875963) }


//$_SERVER['HTTP_HOST'] returns the Host header from the current request.
// $_SERVER['SCRIPT_NAME'] returns the path of the current script:

//-------------------
//==================================================================================================
//                                        GENERATORS
//==================================================================================================

//------------------------
// PHP Iterables  نوعی از متغیر که تابع فورایچ می تونه روش پیمایش انجام بده
// iterable آبجکتی که میتونه به ایتریتور تبدیل شه = > iterator : آبجکتی که میتونی روی تک تک عناصرش پیمایش کنی 

// generator : 

// جنراتور ها زیر مجموعه ای از ایتراتور ها هستند که باعث استفاده کمتر از رم میشن چون موقعیت فعلی رو حفظ میکنن و نیاز نیست مثلا در یک آرایه دوباره آرایه ساخته بشه موقعیت قبلی رو بخاطر میسپاره و  با فراخوانی بعدی ادامه فرآیند رو انجام میده نه از اول 
// میشه به عنوان آرگومان به تابع پاس داده شه مثل زیر
/*
function printIterable(iterable $myIterable)
{


    foreach ($myIterable as $item) {
        echo $item;
    }
}
$arr = ["a", "b", "c"];
printIterable($arr);
*/
/*
 برگشت یک ایترایبل 
return a iterable =>


fucntion getIterable(): iterable {
    return [1,6,90];
}

$myIterable = getIterable();

foreach($myIterable as $item) {
    echo $item;
}

*/
//-------------------

// هاgenerator مثال استفاده نکردن از  : 
// 
/*
function testMemoryArray()
{
    $arr = [];
    $number = 100000000000;
    for ($i = 0; $i < $number; $i++) {
        array_push($arr, $i);
    }
    return $arr;
}
foreach (testMemoryArray() as $num) {
    echo "$num";
}
//testMemoryArray(); // اورو داد چون حافظه کم اومد // Fatal error: Allowed memory size of 134217728 bytes exhausted (tried to allocate 134217736 bytes) in C:\Program Files\Ampps\www\tamrin\tamrin error php.php on line 355
*/

/*
function testMemoryArray()
{

    $number = 10000000;
    for ($i = 0; $i < $number; $i++) {
        yield $i; // تابع با ییلد میتونه چند خروجی داشته باشه
    }
}
foreach (testMemoryArray() as $num) {
    echo "$num"; // 0123456789101112.........
}
var_dump(testMemoryArray()); // object(Generator)#1 (0) { } => یک آبجکت جنراتور ساخته شده است

*/
/*
 متد ها :
 testMemoryArray()->current() // به مقدار فعلی که yield شده دسترسی پیدا میکنیم
 testMemoryArray()->next() //  مقدار بعدی
 testMemoryArray()->getReturn() //  اگر بخوایم پس از اتمام اجرا Generator مقدار return شده ی تابع رو برگردونیم، از این متد استفاده میکنیم
 testMemoryArray()->send() //  مقدار رو به عنوان نتیجه ییلد به جنراتور میفرستیم
*/

//==================================================================================================
//                                        ATTRIBUTES
//==================================================================================================
// PHP 8.0 اضافه شده
//  همون annotations
//  توی زبانهای دیگست که 
//   => اضافه کردن متا دیتا به کلاس‌ها، متدها، متغییرها و … به روش ساختاری

//---------------------------
// ظاهر کلی یک ATTRUBUTE

// use \Support\Attributes\ListensTo;
 
// class ProductSubscriber
// {
//     #[ListensTo(ProductCreated::class)]
//     public function onProductCreated(ProductCreated $event) { /* … */ }
 
//     #[ListensTo(ProductDeleted::class)]
//     public function onProductDeleted(ProductDeleted $event) { /* … */ }
// }
//------------------------

// #[Attribute]
// class ListensTo
// {
//     public string $event;
 
//     public function __construct(string $event)
//     {
//         $this->event = $event;
//     }
// }
//-------------------

// #[Attribute]
// class ListensTo
// {
//     public string $event;
 
//     public function __construct(string $event)
//     {
//         $this->event = $event;
//     }
// }
//-------------------


// #[
//     Route(Http::POST, '/products/create')
//     Autowire
// ]
// class ProductsCreateController
// {
//     public function __invoke() { /* … */ }
// }

// #[
//     Route(Http::POST, '/products/create')
//     Autowire
// ]
// class ProductsCreateController
// {
//     public function __invoke() { /* … */ }
// }
//-------------------
// میشه اونا رو به کلاس و کلاس های بی نام اضافه کرد
// #[ClassAttribute]
// class MyClass { /* … */ }

// $object = new #[ObjectAttribute] class () { /* … */ };
//-------------------

// میتونیم به ثابت ها و متغیرها اضافه اش کنیم 

// #[PropertyAttribute]
// public int $foo;
 
// #[ConstAttribute]
// public const BAR = 1;
//-------------------

// اضافه کردن به متدها و تابع ها

// #[MethodAttribute]
// public function doSomething(): void { /* … */ }
 
// #[FunctionAttribute]
// function foo() { /* … */ }
//-------------------


//-------------------
