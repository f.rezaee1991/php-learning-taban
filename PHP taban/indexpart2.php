//============================================================================================================================
// PHP - 2
//============================================================================================================================
<?php

// ++++++++++++++++ Constants +++++++++++++++++

//define(name, value, case-insensitive) ; 
// define('NATIONAL_KODE', 3920245245, false);
// define('NATIONAL_KODE', 3920245245, true);
// echo NATIONAL_KODE, '<br>';
// echo national_kode;


// const MSG = "you are in my world🧙‍♂️";
// echo MSG, '<br>';
// echo msg; // undefined

// echo constant('MSG');


// $a = 1; /* global scope */

// function test()
// {
//     global $a;
//     echo $a; /* reference to local scope variable */
// }

// test();


// PHP has three types of variable scopes:

// Local variable
// Global variable
// Static variable



// function local_var()
// {
//     $a = 250;  // local variable in function
//     echo $a;
// }
// local_var();
// var_dump($a);



// function mytest()
// {
//     $lang = "PHP";
//     echo "Web development language: " . $lang;
// }
// mytest();
// //using $lang (local variable) outside the function will generate an error  
// echo $lang;  



// $b = 'hasti';
// function sayHello()
// {
//     global $b;
//     echo 'salam' . ' ' . $b;
// }
// sayHello();

// $b = 'hasti';
// function sayHello()
// {

//     echo 'salam' . ' man be to ' . $GLOBALS['b'];
// }
// sayHello();


// function static_var()
// {
//     static $a = 60;
//     $b = 50;
//     $a++;
//     $b++;
//     echo $a, ' ', $b;
// }
// static_var();
// static_var();

// define("PI", 3.14);
// // echo PI;
// define('cars', ['pride', 'bmw', 'benz']);
// // var_dump(cars);
// const ID = 1241;
// // echo ID;
// echo __FILE__;


// runtime vs  compile time

// Magic Constants
// __LINE__

// echo __LINE__; // return current line

// __FILE__
// echo __FILE__; // return file path : C:\Program Files\Ampps\www\PHP taban\index.php

// __DIR__
// echo __DIR__; // direction of file

// __FUNCTION__

// function myfunc()
// {
//     echo __FUNCTION__;
// }
// myfunc();

// __CLASS__
// class PERSON
// {
//     function getCLassName()
//     {
//         echo __CLASS__;
//     }
// }

// $farzad = new PERSON;
// $farzad->getCLassName();
// __TRAIT__
// __METHOD__
// __NAMESPACE__
// ClassName::class



// Expressions ¶ 
// anythin has a value is a expression 
// function golabi()
// {
//     return 85;
// }
// $exp = golabi(); // this is a expresseion


// what is scaler value :  not departed to smaller parts ==> int , float, str, bool 
// non scaler => array , object 


//operators



// $num1 = 148000;
// $num2 = 90;
// echo $num1 + $num2;
// echo '<br>';
// echo $num1 * $num2;
// echo '<br>';
// echo $num1 / $num2;
// echo intdiv($num1, $num2);
// echo '<br>';
// echo $num1 - $num2;
// echo '<br>';
// echo $num1 % $num2;
// echo '<br>';
// echo $num1 ** $num2;
// -------------------------------

// $num1 += 10;
// print_r($num1);
// echo '<br>';

// $num1 **= 10;
// print_r($num1);
// echo '<br>';

// $num1 /= 80;
// print_r($num1);

// $num1 %= 85;
// print_r($num1);

// --------------------------------------

// var_dump('50' == 50);
// var_dump('50' === 50);
// var_dump('50' != 50);
// var_dump('50' !== 50);
// var_dump('50' <> 50);
// var_dump(50 <=> 51);
// var_dump(500 <=> 51);
// var_dump(51 <=> 51);
// ------------------------------------------
// $num1 = 14;
// $num2 = 90;

// var_dump($num1 > $num2);
// var_dump($num1 >= $num2);
// var_dump($num1 < $num2);
// var_dump($num1 <= $num2);

// echo ++$num1;
// echo "<br>";
// echo $num2++;
// -----------------------------------
// convert decimal to bitwise==> bitwise op ==> convert to decimal;


// echo 1 & 1, "<br>";
// echo 0 & 1, "<br>";
// echo 1 & 0, "<br>";
// echo 0 & 0, "<br>";
// // --------------------
// echo 1 | 1, "<br>";
// echo 1 | 0, "<br>";
// echo 0 | 1, "<br>";
// echo 0 | 0, "<br>";
// ------------------------------
// $x = 12;
// $y = 11;
// echo $x ^ $y; xor
// ---------------------------------

// $x = 300;
// $y = "300";
// var_dump($x == $y); // true

// $x = 300;
// $y = "300";
// var_dump($x === $y); // false

// $x = 150;
// $y = "150";
// var_dump($x != $y); // false

// $x = 150;
// $y = "150";
// var_dump($x !== $y); // true 

// $x = 100;
// $y = 300;
// var_dump($x < $y); // true

// ------------------------------
// Error Control Operators ¶ ==> @ 

// $fp = fopen("nosuchfile.txt", "r");
// echo "Hello World \n";

// $fp=@fopen("nosuchfile.txt","r");
// echo "Hello World";
// -------------------------------------
//  backtick ==> ` ` like DOS 

// $output = `ls -al`;
// echo "<pre>$output</pre>";

// ------------------------------------------

// $a = true && false;
// var_dump($a);
// $b = false && true;
// var_dump($b);
// $c = true && true;
// var_dump($c);
// $d = false && false;
// var_dump($d);
// $a = true || false;
// var_dump($a);
// $b = false || true;
// var_dump($b);
// $c = true || true;
// var_dump($c);
// $d = false || false;
// var_dump($d);

// var_dump(true and false);
// var_dump(true or false);
// ---------------------------------------------

// $a = array("a" => "apple", "b" => "banana");
// $b = array("a" => "pear", "b" => "strawberry", "c" => "cherry");
// $c = $a + $b; // Union of $a and $b
// // echo "Union of \$a and \$b : <br />";
// // var_dump($c);
// $c = $b + $a; // Union of $b and $a 
// // echo "<br />Union of \$b and \$a : <br />";
// var_dump($c);


// $a = array("1" => "apple", 0 => "banana");
// $b = array("banana", "apple");
// var_dump($a == $b);
// var_dump($a === $b);


// class FALILY
// {
// }

// $omid = new FALILY;

// if ($omidd instanceof FALILY) {
//     echo ' omid is an instance of FALITY class';
// } else {
//     echo ' omid is NOT an instance of FALITY class';
// }

// -------------------------------------------------------------

// Control Structures ¶

// ----------------------------------------------------------

// $number1 = 6;
// if ($number1 > 0) {
//     echo "{$number1} is greater than 0";
// }


// $number1 = 6;
// if ($number1 < 0) {
//     echo "{$number1} is greater than 0";
// } else {
//     echo "{$number1} is  NOT greater than 0";
// }


// $number1 = 6;
// if ($number1 > 0) {
//     echo "{$number1} is upper than 0";
// } elseif ($number1 == 0) {
//     echo "{$number1} is  equal   0";
// } else {
//     echo "{$number1} is  lower than   0";
// }


// echo $isEven = (86 % 2 == 0) ? 'true' : 'false';


// $x = 90;
// $x;
// $x = null;
// echo $y = $x ?? 100;


// $color = 'red';
// $color = 'blue';
// $color = 'yellow';
// switch ($color) {
//     case 'red':
//         echo ' پرسپولیس سرور استقلاله';
//         break;
//     case 'blue':
//         echo " استقلال سرور پرسپولیسه";
//         break;
//     default:
//         echo " 😋فقط تراکختور ⚽⚽💎";
// }


/* 
<?php if ($a == 5): ?>
A is equal to 5
<?php endif; ?>
*/


// $num = 10;
// while ($num >= 0) {
//     echo " number is $num <br>";
//     $num--;
// }


// $x1 = 1;
// do {
//     echo "Increment Number : $x1 <br />";
//     echo "Hello World  <br />";
//     $x1 = $x1 + 1;
// } while ($x1 <= 5);


// for ($x = 0; $x <= 30; $x += 2) {
//     echo "my even number is : $x <br>";
// }


// $fruits = array("Orange", "Apple", "Banana",  "Cherry", " Mango");


// foreach ($fruits as $zendegi) {
//     echo " $zendegi kheili khoshmaze ast <br>";
// }


// $roll = rand(1, 6);
// if ($roll == 6) echo " winner 🎉 your number is $roll ";
// else echo "try again your number is $roll";


// match in php 8 :

// $name = 'farzad';
// $msg = match ($name) {
//     'farzad' => ' you are a good man',
//     'abbas' => ' you are professional',
//     'fatemeh' => 'you are nice',
//     default => 'you are a human',
// };
// echo $msg;

// $number = 2;
// $name = match ($number) {
//     1 => 'One',
//     2 => 'Two',
// };
// echo $name;
