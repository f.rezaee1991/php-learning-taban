<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <!-- +++++++++++++++ Basic syntax ¶ +++++++++++++++++  -->


    <!-- <p>this is pharagraph1</p>
    // <?PHP echo 'this is foo from PHP' ?>
    <p>this is pharagraph2</p> -->

    <!-- 

    <?PHP if (0) : ?>
        show if is true
        show if is true
        show if is true
    <?PHP else : ?>
        show when if in false
        show when if in false
        show when if in false
        show when if in false
    <?PHP endif ?> -->

    <!-- 
    <?php echo 'salam '; ?>
    inja Iran ast.
    <?= "But newline now" ?> -->


    <?php
    // comment
    # another comment

    // $x = 5 /* + 15 */ + 5;
    // echo $x;
    // 
    ?>





</body>

</html>




<?php



// echo 'salam Tabane Shahr';

// $firstName = "Farzad";
// $lastName = "Rezaei";

// echo $firstName . ' ' . $lastName;





// 2              +++++++++++++  tyepes +++++++++++ 

// gettype()
// $a_bool = TRUE;   // a boolean
// $a_str  = "foo";  // a string
// $a_str2 = 'foo';  // a string
// $an_int = 12;     // an integer

// echo gettype($a_bool); // prints out:  boolean


// $a = 1234; // decimal number
// $a = 0123; // octal number (equivalent to 83 decimal)
// $a = 0x1A; // hexadecimal number (equivalent to 26 decimal)
// $a = 0b11111111; // binary number (equivalent to 255 decimal)
// $a = 1_234_567; // decimal number (as of PHP 7.4.0)

// intdiv() // int division

// echo (int)14.5; // 14

// var_dump(25 / 7);         // float(3.5714285714286)
// var_dump((int) (25 / 7)); // int(3)
// var_dump(round(25 / 7));  // float(4)




// $firstName = "farzad"; // string
// $age = 25; // int
// $weight = 75.2; //string
// $isKindPerson = true; //boolean
// $days = array('sat', 'sun', 'mond', 'friday', false, 14); // array
// $greedy = null; // null

// class person
// {
//     function sayHello()
//     {
//         echo 'salam dooste man';
//     }
// }

// $person1 = new person; // object
// var_dump($days);
// echo gettype($age);


// $foo = '50kg';
// settype($foo, 'integer');
// settype($foo, 'bool');
// echo $foo;
// echo gettype($foo);
// echo settype($foo, 'string');
// echo settype($foo, 'bool');

// $mooz = True;
// $mooz = 450;
// $shalil = true;
// if ($mooz === true) {
//     echo 'if is true';
// };

//boolean :


// $number = 150;
// (bool) $number;
// echo (bool) $number;

// var_dump((bool) "");
// $var1 =  (bool) '14';
// echo $var1; // 1
// var_dump((bool) ""); // false
// var_dump((bool) 'today'); // true
// var_dump((bool) array());  // false
// var_dump((bool) null); //false
// var_dump((bool) "-5.5"); //true
// var_dump((bool) "0"); //false
// var_dump((bool) "0.0"); //true
//-----------------------------------------------

// integer :


// $a_decimal = 120;
// $b_octal = 0777;
// $c_hexa = 0xe15f;
// // $d = 12_25_1200;  
// echo $a_decimal, ' ',  $b_octal;


// $num1 = 2000000000;
// // var_dump($num);
// $num2 = 20000000000000000000000000;
// // var_dump($num);
// $num3 = -53;

// echo intdiv($num1, $num3);
// echo intdiv(12, 5);

// var_dump(round((5.7))); // 6
// var_dump(round(5.790, 2));
// var_dump(round(5.793, 2));
// var_dump(round(5.798, 2));


// echo intval(80);
// echo intval(80.5);
// echo intval(80.9);
// echo intval('80.9');
// echo intval('80.9sdf');
// echo intval('sdf');
// echo intval(042);
// echo intval(true);
// echo intval('NaN');

//string:
// $s = " hello \n my name is Farzad";
// $s = " hello \n my name is Farzad";
// echo $s;
// echo 'This will not expand: \n a newline';
// echo "This will not expand: \n a newline";


// array :

// $myArray = [1, 3, 5];
// $myArray2 = array(2, 4, 6);
// var_dump($myArray);
// $myArray2[1] = 40;
// var_dump($myArray2);
// $myArray3 = array(
//     "name" => "farzad",
//     "age" => 25,
//     "isActive" => true
// );
// var_dump($myArray3);
// $evens = [
//     1 => 2,
//     2 => 4,
//     3 => 6
// ];
// var_dump($evens);
/* 
$array = array(
    1    => 'a',
    '1'  => 'b', // the value "a" will be overwritten by "b"
    1.5  => 'c', // the value "b" will be overwritten by "c"
    -1 => 'd',
    '01'  => 'e', // as this is not an integer string it will NOT override the key for 1
    '1.5' => 'f', // as this is not an integer string it will NOT override the key for 1
    true => 'g', // the value "c" will be overwritten by "g"
    false => 'h',
    '' => 'i',
    null => 'j', // the value "i" will be overwritten by "j"
    'k', // value "k" is assigned the key 2. This is because the largest integer key before that was 1
    2 => 'l', // the value "k" will be overwritten by "l"
);
var_dump($array)
*/

/* 

// Create a simple array.
$array = array(1, 2, 3, 4, 5);
print_r($array);

// Now delete every item, but leave the array itself intact:
foreach ($array as $i => $value) {
    unset($array[$i]);
}
print_r($array);

// Append an item (note that the new key is 5, instead of 0).
$array[] = 6;
print_r($array);

// Re-index:
$array = array_values($array);
$array[] = 7;
print_r($array);
*/

/* 
$a = array(1,4,6,7,5);


echo count($a); // تعداد اعضای آرایه
echo sizeof($a);

$a = array(1,4,6,7,5);
list($var1,$var2,$var3) = $a;
echo $var1, $var2, $var3;


$a = array(1,4,6,7,5);
$b = $a;

sort($a);
var_dump($b); آ سورت شد اما بی سورت نشد


$a=array("red","green","blue");
array_pop($a);
print_r($a);  حذف آخرین عنصر


$a=array("red","green");
array_push($a,"blue","yellow");
print_r($a); اضافه کردن به آخر آرایه



Do not quote keys which are constants or variables, as this  زمانی که کلید متغیر یا ثابت باشد داخل کوت نمیذاریم
*/


//------------------------
// PHP Iterables  نوعی از متغیر که تابع فورایچ می تونه روش پیمایش انجام بده
// iterable آبجکتی که میتونه به ایتریتور تبدیل شه = > iterator : آبجکتی که میتونی روی تک تک عناصرش پیمایش کنی 
// جنراتور ها زیر مجموعه ای از ایتراتور ها هستند که باعث استفاده کمتر از رم میشن چون موقعیت فعلی رو حفظ میکنن و نیاز نیست مثلا در یک آرایه دوباره آرایه ساخته بشه موقعیت قبلی رو بخاطر میسپاره و  با فراخوانی بعدی ادامه فرآیند رو انجام میده نه از اول 
// میشه به عنوان آرگومان به تابع پاس داده شه مثل زیر
/*
function printIterable(iterable $myIterable)
{


    foreach ($myIterable as $item) {
        echo $item;
    }
}
$arr = ["a", "b", "c"];
printIterable($arr);
*/
/*
 برگشت یک ایترایبل 
return a iterable =>


fucntion getIterable(): iterable {
    return [1,6,90];
}

$myIterable = getIterable();

foreach($myIterable as $item) {
    echo $item;
}

*/


 /*  onject  // آبجکت To create a new object, use the new statement to instantiate a class:

class Foo {
    function bar(){
        echo ' we are in bar function';
    }
}
 $fooo = new Foo();
 fooo->bar();
 
 */
/*  NULL ¶ متغیر // متغیری از نوع NULL، فقط یک مقدار دارد و آن هم مقدار NULL میباشد.


The special null value represents a variable with no value. null is the only possible value of type null.

A variable is considered to be null if:

it has been assigned the constant null. // ثابت نال

it has not been set to any value yet. // هنوز مقداری به متغیر ست نشده

it has been unset(). / استفاده از تابع آنست

*/

/* 
نوع Resource:

متغیر resource، منبعی است که خروجیِ توابعی مانند باز کردن فایل، اتصال از طریق ftp، اتصال به دیتابیس، اجرای کوئری و ... را در خود نگه میدارد. 
مثال زیر را در نظر بگیرید:

<?php
$file = fopen('filename.txt', 'w');
var_dump($file); // resourceCopy
در مثال بالا، با استفاده از دستور fopenفایلی را در حالت Write (نوشتن) باز کرده ایم. خروجی این تابع، یک resource است. (در این لینک لیست توابعی که resource تولید میکنند، قرار دارد.)
به این resource نمیتوان بصورت مستقیم، دسترسی داشت بلکه باید از توابع آن استفاده کرد.
برای مثال اگر بخواهم، داخل فایل چیزی بنویسم و فایل را ببندم:

<?php
$rs = fopen('filename.txt', 'w');
fwrite($rs, 'بیت هاب');
fclose($rs);Copy
 */


 /* 
 9. نوع Callback / Callable:

وقتی نام تابعی را بعنوان یک آرگومان به توابع دیگر ارسال میکنید، تا در پشت آن توابع، صدا زده شود و نهایتاً کدهایش اجرا شود، عمل callback انجام شده است.
پس تابعی که نامش بعنوان پارامتر ارسال میشود، تابع callback است و آن تابعی که تابع callback را صدا میزند، اصطلاحاً callback executer میگوئیم.

callback به معنیه called at the back میباشد.
برای درک موضوع، به مثال زیر توجه کنید:

<?php
// The callback function
function myCallback()
{
    echo 'Hello World!';
}

// The callback executer
function executeCallback($callback)
{
    // call the callback
    $callback();
}

executeCallback('myCallback'); // Hello World!Copy.
اگر به مثال توجه کنید میبینید، این نوع از داده مفهوم ساده ای دارد. البته مبانی توابع را هنوز نگفته ایم و فقط هدفمان، معرفی انواع داده ها بوده است.
در مثال زیر، callback در یکی از توابع خود PHP اتفاق می افتد:

<?php
// The callback function
function myCallback()
{
    echo 'Hello World!';
}

// Simple callback
call_user_func('myCallback'); // Hello World!Copy
تابع call_user_func() توابع callback ای که ساخته اید را بعنوان پارامتر دریافت کرده و اجرا میکند.
نتیجه ی اجرای این کد، مانند مثال قبلی میباشد.
*/
// 3

// $str1 = "this is how we do";

// $msg = <<< EOD
// salam man 
// be to 
// yare ghadimi
// EOD;

// // echo $msg;

// $alphabet = 'abcdefghij';

// $s = substr($alphabet, 2, 3);
// $length = strlen($alphabet);
// $wordsCount = str_word_count($msg);
// $replaysedMsg = str_replace('ghadimi', 'jadid', $msg);
// // echo $s;
// // echo $length;
// // echo $wordsCount;
// echo $replaysedMsg;



// 4 5 string functions





// $msg2 = " this is a string   ";
// $str = "Hello World!";
// echo $str . "<br>";
// echo trim($str, "Hed!");

// echo '<br>';
// echo strpos($msg2, 'a');


// $msg3 = "your grade is A";

// $isGradeA = strpos($msg3, "A");
// if ($isGradeA) {
//     echo "passed";
// } else {
//     echo "faild";
// }

// echo strstr('wellcom to my page', 'to');

// echo strtolower($msg3);
// echo strtoupper($msg3);

// $msg3 = "your grade is A";

// var_dump(explode(' ', $msg3));
// print_r(explode(' ', $msg3));


// $days = array('sat', 'sun', 'mond', 'friday', false, 14); // array
// echo implode(' ', $days);


// $firstString = ' type here to searchhhhhhh';
// $secondString = ' type here to searchhhhhhHhhh';
// echo strcmp($firstString, $secondString);
