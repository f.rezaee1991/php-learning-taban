<?PHP
// echo 'hi';


// +============= functions ===============+


// function sayHeloo($name)
// {
//     echo "salam $name  khosh umadi ";
// }
// sayHeloo("Hasan");


// sayHeloo("Hasan");
// function sayHeloo($name)
// {
//     echo "salam $name  khosh umadi ";
// }


// $isSchoolOpen = true;
// $isCorona = false;
// if ($isSchoolOpen && !$isCorona) {
//     function goToSchool()
//     {
//         echo 'school starts at 8 am';
//     }
// }
// goToSchool();

// $isSchoolOpen = true;
// $isCorona = true;
// if ($isSchoolOpen && !$isCorona) {
//     function goToSchool()
//     {
//         echo 'school starts at 8 am';
//     }
// }
// goToSchool(); // nemitoonim farakhani konim


// تابع بازگشتی
// چاپ اعداد 1 تا 20 بصورت بازگشتی 
// $n = 20;
// $i = 1;
// function myRecursive()
// {
//     global $n, $i;

//     if ($i <= $n) {

//         echo $i, " <br>";
//         $i++;
//         myRecursive();
//     } else {
//         echo 'end';
//     }
// }
// myRecursive();

// function recursion($a)
// {
//     if ($a < 20) {
//         echo "$a\n";
//         recursion($a + 1);
//     }
// }
// recursion(12);


// type declaration
// function sumNumbers(float $num1, float $num2)
// {
//     echo $num1 + $num2;
// }
// sumNumbers(12, 18); // 30
// sumNumbers(12, '18sdfsdf'); // 30
// sumNumbers(12, 'ali'); //  Fatal error: Uncaught TypeError: Argument 2 passed to sumNumbers() must be of the type float


//defualt argument value

// function sayHello($firstName = 'aziz')
// {
//     echo "khosh umadi $firstName jan";
// }
// sayHello('javad'); // khosh umadi javad jan
// sayHello(); // khosh umadi aziz jan


//return type declaration
// function sumNumbers(float $num1, float $num2): string
// {
//     return $num1 + $num2;
// }
// echo sumNumbers(12, 18.5); // "30.5 " 


//call by referance  => مقدار واقعی متغیری که به تابع پاس داده شده است تغییر می کند
// & استفاده از

// function fun(&$a)
// {
//     $a++;
// }

// $a = 10;
// fun($a);
// echo $a;  // 11

//call by value => تغییرات متغیر فقط درون خود تابع است و مقدار اصلی متغیر در بیرون از تابع تغییر نمی کند

// function fun($a)
// {
//     $a++;
// }

// $a = 10;
// fun($a);
// echo $a;  // ثابت می مانند  10


// مشخص کردن نوع خروجی تابع
// function fun(int $x, int $y) : string {
//     return $x * $y;
// }
// var_dump(fun(5,6)); // string(2) "30" 

// تعریف ثابت
// define("PI", 3.141592); 
// echo PI;
// PI = 2;  // نمیشه تغییرش داد

// استفاده از die()  exit() 
// برنامه رو متوقف میکنه و همین کد رو اجرا میکنه برای دیباگینگ خوبه
// if(true){
//     die('error');
// }

// unset() => برای حذف متغیر یا یک عنصر آرایه 
// isset() => برای چک کردن اینکه یک متغیر وجود داره یا نه 
//Check whether a variable is empty. Also check whether the variable is set/declared:

// is_int();  تشخیص نوع
// is_float();
// is_object();
// is_array();
// ... 


// variable length arguments  : استفاده از funct_get_args()   ,    func_num_args() 

// برنامه ای بنویسید که  میانگین  هر تعداد  اعداد داده شده را حساب کند  

// function ave()
// {
//     // func_num_args()
//     // echo func_get_args();
//     // var_dump(func_get_args());
//     $count = func_num_args();
//     if ($count == 0) echo 'لطفا اعداد را وارد کنید';
//     else {
//         $numbers = func_get_args();
//         $sum = 0;
//         foreach ($numbers as $number) {
//             $sum += $number;
//             // echo $number;
//         }

//         return $sum / $count;
//     }
// }
//echo ave(); // لطفا اعداد را وارد کنید
// echo ave(10, 20, 30); // 20


// =============================================================
//OOP
//==============================================================

// procedural vs oop
// procedural : <?php controller 
//             --------
//             --------
//             model
//             ----------
//             -----------

//             view
//             ---------

// پشت سر هم

// oop : controller ==> { ----
//                             ----
//                             }
// //       model      ==>
//       view       ==>

// هر بخش بصورت مجزا بررسی میشه


// تعریف کلاس
// class Person
// {
//     // properties and methods 
// }
// $farzad = new Person; // make instance
// var_dump($farzad);  // object(Person)#1 (0) { }

// class Person
// {
//     // properties and methods
//     public $info = "this is info property";
// }
// $farzad = new Person; // make instance
// var_dump($farzad);  // object(Person)#1 (1) { ["info"]=> string(21) "this is info property" }


// class Adam
// {
//     private $name;
//     private $age;
// }
// $noori = new Adam;

// class Animal
// {
//     // private function run()
//     public function run()
//     {
//         $a = "run fast";
//         return $a;
//     }
// }
// $boz = new Animal;
// echo $boz->run();  // وقتی که private باشد میتونی دسترسی داشته باشی 


// class Adam
// {
//     private $name = 'hazrate adam👳‍♂️ ';
//     private $age = 1000000000; //i dont khnow
//     public function run()
//     {
//         $a = $this->name;
//         return $a;
//     }
// }
// $noori = new Adam;

// class Animal
// {
//     // private function run()
// }
// echo $noori->run(); //با اینکه متغیر پرایویت است اما با متد پابلیک بهش دسترسی پیدا کردیم یی


// ارث بری

// class Adam
// {
//     // private $name = 'hazrate adam👳‍♂️ ';
//     protected $name = 'hazrate adam👳‍♂️ ';  // فقط داخل خود کلاس و  کلاس هایی که از آن ارث میبرن دسترسی هست
//     private $age = 1000000000; //i dont khnow

// }
// $noori = new Adam;

// class JavanMards extends Adam
// {
//     public function run()
//     {
//         $a = $this->name;
//         return $a;
//     }
// }
// $javanMardeGhasab = new JavanMards;
// echo $javanMardeGhasab->run();  //  hazrate adam👳‍♂️ 

// class Person
// {
//     // properies
//     public $name;
//     public $eyeColor;
//     public $age;

//     //methods
//     public function setName($newName)
//     {
//         $this->name = $newName;
//     }
// }

// $person1 = new Person();
// echo $person1->name; // چیزی نمایش نمیده چون هنوز نامی ست نشده

// $person1->setName('Amirhosein'); // نام ست شد - فقط در آبجکت نه در کلاس
// echo $person1->name; //  Amirhosein


// متد سازنده  => به محض ساخته شدن آبجکت فراخوانی می شود تابع ریترن ندارد

// class Person
// {
//     public $name;
//     public $eyeColor;
//     public $age;

//     public function __construct($name, $eyeColor, $age)
//     {
//         $this->name = $name;
//         $this->eyeColor = $eyeColor;
//         $this->age = $age;
//     }


//     public function setName($newName)
//     {
//         $this->name = $newName;
//     }
//     public function getName() // پروپرتی اسم رو میتونی پرایوت کنی و با این متد اسم رو بگیری بدون دستکاری در پروپرتی اسم
//     {
//         return $this->name;
//     }
// }
// $khorzookhan = new Person('khorzookhan', 'black', 100);
// echo $khorzookhan->name;
// echo $khorzookhan->age;
// echo $khorzookhan->getName();


// DESTRUCTOR => به محض از 
// دیستراکتور که زمانی فراخوانی می‌شود که یک آبجکت از بین می‌رود و از جمله مواقعی که یک شیئ از بین می‌رود می‌توان به زمانی اشاره کرد که اسکریپت ما به اِتمام رسیده که بالتبع بخشی از حافظهٔ اِشغال‌شده توسط آن اسکریپت نیز آزاد می‌شود (همچنین زمانی که از متدی تحت عنوان ()unset استفاده می‌کنیم، آبجکت ساخته‌شده از بین خواهد رفت.) 

// class Person
// {
//     public $name;
//     public $eyeColor;
//     public $age;

//     public function __construct($name, $eyeColor, $age) // متد سازنده
//     {
//         $this->name = $name;
//         $this->eyeColor = $eyeColor;
//         $this->age = $age;
//     }

//     public function __destruct()
//     {
//         echo " تابع ویرانگر فراخوانی شد";
//     }

//     public function setName($newName)
//     {
//         $this->name = $newName;
//     }
//     public function getName() // پروپرتی اسم رو میتونی پرایوت کنی و با این متد اسم رو بگیری بدون دستکاری در پروپرتی اسم
//     {
//         return $this->name;
//     }
// }
// $gholam = new Person('khorzookhan', 'black', 100);
// unset($gholam); //  " تابع ویرانگر فراخوانی شد"

// -----------------------------------------------
// Static Properties And Methods In OOP PHP
//  => بدون آنکه شی ای از یک کلاس بسازیم، به متدها و متغیرهای آن کلاس دسترسی پیدا کنیم از استاتیک استفاده می کنیم static

// (::), is a token which allows access to static, constant, and overridden properties or methods of a class.


// class MyClass
// {

//     private static $static_property;

//     public static function static_method()
//     {
//         //...
//     }
//     public function method()
//     {
//         self::$static_property; // برای دسترسی از داخل کلاس بجای کلمه دیس از سلف استفاده میکنیم this   self 
//         self::static_method();
//     }
// }
// MyClass::$static_property; // دسرسی به پروپرتی استاتیک بدون ساختن آبجکت از کلاس

//-----------------------------------------------
// Interfaces  رابط ها : باعث میشن که کلاس رو موظف به پیاده سازی از متدهای دلخواه که توی اینترفیس تعریف میشن کنیم


// interface interfaceName
// {
//     // متود های انتزاعی در این قسمت قرار می گیرند
// }

// class Child implements interfaceName  /* کلاس میتونه از چند تا اینترفیس پیروی کنه*/
// {
//     // این قسمت متد های اینترفیس را تعریف می کند و ممکن است کد های خاص خودش را نیز داشته باشد
// }

// interface Car
// {
//     public function setModel($name);

//     public function getModel();
// }


// class miniCar implements Car
// {
//     private $model;

//     public function setModel($name)
//     {
//         $this->model = $name;
//     }

//     public function getModel()
//     {
//         return $this->model;
//     }
// }


// ------------------------------------------
// abstract class  کلاس انتزاعی
// => کلاسی به نام کلاس پدر خواهیم داشت اما آن کلاس به شیء تبدیل نخواهد شد چرا که به خودِ آن کلاس نیازی نداریم.

// // نمونه ای از یک کلاس و متدِ انتزاعی
// abstract class Car
// {
//     abstract public function calcNumMilesOnFullTank();
// }


// abstract class Ch2_Product
{

//     // خصوصیات در این قسمت تعریف شده اند
//     protected $_type;
//     protected $_title;

//     // متدها در این قسمت تعریف شده اند
//     public function getProductType()
//     {
//         return $this->_type;
//     }
//     public function getTitle()
//     {
//         return $this->_title;
//     }
//     abstract public function display();
// }

// // کلاس فرزند
// public function display()
// {
// echo "<p>Book: $this->_title ($this->_pageCount pages)</p>";
// }

// ----------------------------------
// Clone کردن یک آبجکت (تولید مثل)

// class test
// {
// public $a;
// private $b;
// function __construct($a, $b)
// {
// $this->a = $a;
// $this->b = $b;
// }
// }
// $a = new test("ankur" , "techflirt");
// $b = $a; //Copy of the object
// $c = clone $a; //clone of the object استفاده از کلمه 
// $a->a = "no Ankur";
// print_r($a);
// print_r($b);
// print_r($c);


// class test
// {
// public $a;
// private $b;
// function __construct($a, $b)
// {
// $this->a = $a;
// $this->b = $b;
// }
// function __clone()  // متد clone__ را ساختیم پس هر موقع عمل کپی توسط clone انجام شود این متد به صورت اتوماتیک اجرا شده پس در نتیجه مقدار متغیر C همان c می باشد.
// {
// $this->a = "c";
// }
// }
// $a = new test("ankur" , "techflirt");
// $b = $a; //Copy of the object
// $c = clone $a; //clone of the object
// $a->a = "no Ankur";
// print_r($a);
// print_r($b);
// print_r($c);
// print_r($a);




// ================================================================================
// nameSpace 
// ================================================================================

// فضای نام  برای استفاده بهتر از نامگذاری استفاده میشه و ما میتونیم فضایی رو برای تعریف نام ها تعریف کنیم این امکان رو میده که مثلا بتونیم یک کتابخانه رو دانلود کنیم و بدون اینکه مشکل یکسان بودن نام ها داشته باشیم ازش استفاده کنیم

// حتما باید قبل از سایر کد ها در یک فایل پی اچ پی نوشته بشه 

// namespace one;

// function name(){
//     return 'farzad';
// }

// echo \tow\name();
// // ----------------
// namespace two ;
// function name(){
//     return "ali";
// }

//  می توانند با گروه بندی کلاسهایی که برای انجام یک کار با هم کار می کنند ، سازماندهی بهتری داشته باشند
// آنها اجازه می دهند از همین نام برای بیش از یک کلاس استفاده شود


// <?php
// namespace MyNamspaceName {    قبل از هر دستور دیگری
   
//     // Regular PHP code 
//     function hello() 
//     {
//         echo 'Hello I am Running from a namespace!';
//     }
// }

// 
// <?php 
// namespace {  بدون نام  گلوبال است
   
    // Global space!
// }


//<?php  میشه در یک فایل چند نیم اسپیس تعریف کرد
// namespace MyNamespace1 {
   
// }
    
// namespace MyNamespace2 {
   
// }
    
// namespace {
   
// }


// <?php
// namespace MyNamespaceName;
// function hello()
// 	{
// 		echo 'Hello I am Running from a namespace!';
// 	}
	
// // Resolves to MyNamespaceName\hello   از بک اسلش استفاده میکنیم 
// hello();

// // Explicitly resolves to MyNamespaceName\hello
// namespace\hello();


// <?php
// namespace MyNamespaceName;
	
// require 'project/database/connection.php';
	
// use Project\Database\Connection as Connection;
	
// $connection = new Connection();
	
// use Project\Database as Database;   برای استفاده  از کلمه یوز استفاده کینم برای ساده کردن استفاده
	
// $connection = new Database\Connection();

